package ru.tsk.vkorenygin.tm.api.service;

import ru.tsk.vkorenygin.tm.entity.Project;
import ru.tsk.vkorenygin.tm.enumerated.Status;
import ru.tsk.vkorenygin.tm.exception.AbstractException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyNameException;

import java.util.Optional;

public interface IProjectService extends IOwnerService<Project> {

    void create(final String name, final String userId) throws EmptyNameException;

    void create(final String name, final String description, final String userId) throws AbstractException;

    Optional<Project> findByName(final String name, final String userId) throws EmptyNameException;

    Project changeStatusById(final String id, final Status status, final String userId) throws AbstractException;

    Project changeStatusByName(final String name, final Status status, final String userId) throws AbstractException;

    Project changeStatusByIndex(final Integer index, final Status status, final String userId) throws AbstractException;

    Project startById(final String id, final String userId) throws AbstractException;

    Project startByIndex(final Integer index, final String userId) throws AbstractException;

    Project startByName(final String name, final String userId) throws AbstractException;

    Project finishById(final String id, final String userId) throws AbstractException;

    Project finishByIndex(final Integer index, final String userId) throws AbstractException;

    Project finishByName(final String name, final String userId) throws AbstractException;

    Project updateById(final String id,
                       final String name,
                       final String description,
                       final String userId) throws AbstractException;

    Project updateByIndex(final Integer index,
                          final String name,
                          final String description,
                          final String userId) throws AbstractException;

    Optional<Project> removeByName(final String name, final String userId) throws EmptyNameException;

}
