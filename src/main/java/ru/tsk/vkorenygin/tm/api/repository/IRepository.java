package ru.tsk.vkorenygin.tm.api.repository;

import ru.tsk.vkorenygin.tm.entity.AbstractEntity;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyIdException;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface IRepository<E extends AbstractEntity> {

    E add(final E entity);

    void addAll(final Collection<E> collection);

    boolean existsById(final String id);

    boolean existsByIndex(final Integer index);

    int getSize();

    List<E> findAll();

    List<E> findAll(final Comparator<E> comparator);

    Optional<E> findById(final String id) throws EmptyIdException;

    Optional<E> findByIndex(final Integer index);

    void clear();

    Optional<E> removeById(final String id) throws EmptyIdException;

    Optional<E> removeByIndex(final Integer index);

    Optional<E> remove(final E entity);

}
