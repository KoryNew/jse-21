package ru.tsk.vkorenygin.tm.api.service;

import ru.tsk.vkorenygin.tm.entity.Task;
import ru.tsk.vkorenygin.tm.enumerated.Status;

import java.util.Optional;

public interface ITaskService extends IOwnerService<Task> {

    void create(final String name, final String userId);

    void create(final String name, final String description, final String userId);

    Optional<Task> findByName(final String name, final String userId);

    Task changeStatusById(final String id, final Status status, final String userId);

    Task changeStatusByName(final String name, final Status status, final String userId);

    Task changeStatusByIndex(final Integer index, final Status status, final String userId);

    Task startById(final String id, final String userId);

    Task startByIndex(final Integer index, final String userId);

    Task startByName(final String name, final String userId);

    Task finishById(final String id, final String userId);

    Task finishByIndex(final Integer index, final String userId);

    Task finishByName(final String name, final String userId);

    Task updateById(final String id,
                    final String name,
                    final String description,
                    final String userId);

    Task updateByIndex(final Integer index,
                       final String name,
                       final String description,
                       final String userId);

    Optional<Task> removeByName(final String name, final String userId);

}
