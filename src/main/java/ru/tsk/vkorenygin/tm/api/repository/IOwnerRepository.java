package ru.tsk.vkorenygin.tm.api.repository;

import ru.tsk.vkorenygin.tm.entity.AbstractOwnerEntity;
import ru.tsk.vkorenygin.tm.exception.AbstractException;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface IOwnerRepository<E extends AbstractOwnerEntity> {

    E add(final E entity) throws AbstractException;

    boolean existsById(final String id, final String userId);

    boolean existsByIndex(final Integer index, final String userId);

    List<E> findAll(final String userId);

    List<E> findAll(final Comparator<E> comparator, final String userId);

    Optional<E> findById(final String id, final String userId) throws AbstractException;

    Optional<E> findByIndex(final int index, final String userId) throws AbstractException;

    Optional<E> removeById(final String id, final String userId) throws AbstractException;

    Optional<E> removeByIndex(final int index, final String userId) throws AbstractException;

    Optional<E> remove(final E entity, final String userId) throws AbstractException;

    void clear(String userId);

}

