package ru.tsk.vkorenygin.tm.api.repository;

import ru.tsk.vkorenygin.tm.entity.Project;
import ru.tsk.vkorenygin.tm.enumerated.Status;

import java.util.Optional;

public interface IProjectRepository extends IOwnerRepository<Project> {

    int getSize();

    Optional<Project> findByName(final String name, final String userId);

    Project changeStatusById(final String id, final Status status, final String userId);

    Project changeStatusByName(final String name, final Status status, final String userId);

    Project changeStatusByIndex(final Integer index, final Status status, final String userId);

    Project startById(final String id, final String userId);

    Project startByIndex(final Integer index, final String userId);

    Project startByName(final String name, final String userId);

    Project finishById(final String id, final String userId);

    Project finishByIndex(final Integer index, final String userId);

    Project finishByName(final String name, final String userId);

    Optional<Project> removeByName(final String name, final String userId);

}
