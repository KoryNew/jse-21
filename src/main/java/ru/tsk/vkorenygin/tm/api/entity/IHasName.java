package ru.tsk.vkorenygin.tm.api.entity;

public interface IHasName {

    String getName();

    void setName(final String name);

}
