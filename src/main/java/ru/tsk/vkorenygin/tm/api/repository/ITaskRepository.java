package ru.tsk.vkorenygin.tm.api.repository;

import ru.tsk.vkorenygin.tm.entity.Task;
import ru.tsk.vkorenygin.tm.enumerated.Status;

import java.util.List;
import java.util.Optional;

public interface ITaskRepository extends IOwnerRepository<Task> {

    int getSize();

    Optional<Task> findByName(final String name, final String userId);

    Task changeStatusById(final String id, final Status status, final String userId);

    Task changeStatusByName(final String name, final Status status, final String userId);

    Task changeStatusByIndex(final Integer index, final Status status, final String userId);

    Task startById(final String id, final String userId);

    Task startByIndex(final Integer index, final String userId);

    Task startByName(final String name, final String userId);

    Task finishById(final String id, final String userId);

    Task finishByIndex(final Integer index, final String userId);

    Task finishByName(final String name, final String userId);

    Task bindTaskToProjectById(final String projectId, final String taskId, String userId);

    Task unbindTaskById(final String id, final String userId);

    List<Task> findAllByProjectId(final String id, final String userId);

    void removeAllTaskByProjectId(final String id, final String userId);

    Optional<Task> removeByName(final String name, final String userId);

}
