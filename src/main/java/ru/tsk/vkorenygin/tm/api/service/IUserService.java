package ru.tsk.vkorenygin.tm.api.service;

import ru.tsk.vkorenygin.tm.entity.User;
import ru.tsk.vkorenygin.tm.enumerated.Role;
import ru.tsk.vkorenygin.tm.exception.AbstractException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyEmailException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyIdException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyLoginException;

import java.util.Optional;

public interface IUserService extends IService<User> {

    boolean isLoginExists(final String login) throws EmptyLoginException;

    boolean isEmailExists(final String email) throws EmptyLoginException, EmptyEmailException;

    User create(final String login, final String password) throws AbstractException;

    User create(final String login, final String password, final String email) throws AbstractException;

    User create(final String login, final String password, final Role role) throws AbstractException;

    Optional<User> findByLogin(final String login) throws EmptyLoginException;

    Optional<User> findByEmail(final String email) throws EmptyEmailException;

    Optional<User> removeByLogin(final String login) throws EmptyLoginException;

    User setPassword(final String userId, final String password) throws AbstractException;

    User updateUser(final String userId, final String firstName, final String lastName, final String middleName) throws EmptyIdException;

}
