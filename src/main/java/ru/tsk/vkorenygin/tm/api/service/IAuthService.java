package ru.tsk.vkorenygin.tm.api.service;

import ru.tsk.vkorenygin.tm.entity.User;
import ru.tsk.vkorenygin.tm.exception.AbstractException;
import ru.tsk.vkorenygin.tm.exception.user.AccessDeniedException;

public interface IAuthService {

    User getUser() throws AbstractException;

    String getUserId() throws AccessDeniedException;

    boolean isAuth();

    void logout();

    void login(final String login, final String password) throws AbstractException;

    void registry(final String login, final String password, final String email) throws AbstractException;

}
