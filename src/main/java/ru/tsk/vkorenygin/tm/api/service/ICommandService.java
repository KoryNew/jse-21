package ru.tsk.vkorenygin.tm.api.service;

import ru.tsk.vkorenygin.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService {

    Collection<AbstractCommand> getCommands();

    Collection<AbstractCommand> getArguments();

    Collection<String> getCommandNames();

    Collection<String> getCommandArgs();

    AbstractCommand getCommandByName(final String name);

    AbstractCommand getCommandByArg(final String name);

    void add(final AbstractCommand command);

}
