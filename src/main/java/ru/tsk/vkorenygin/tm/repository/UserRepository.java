package ru.tsk.vkorenygin.tm.repository;

import ru.tsk.vkorenygin.tm.api.repository.IUserRepository;
import ru.tsk.vkorenygin.tm.entity.User;

import java.util.Optional;
import java.util.function.Predicate;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    protected Predicate<User> hasLogin(String login) {
        return e -> login.equals(e.getLogin());
    }

    protected Predicate<User> hasEmail(String email) {
        return e -> email.equals(e.getEmail());
    }

    @Override
    public Optional<User> findByLogin(final String login) {
        return entities.stream()
                .filter(hasLogin(login))
                .findFirst();
    }

    @Override
    public Optional<User> findByEmail(final String email) {
        return entities.stream()
                .filter(hasEmail(email))
                .findFirst();
    }

    @Override
    public Optional<User> removeByLogin(final String login) {
        Optional<User> user = findByLogin(login);
        user.ifPresent(e -> entities.remove(e));
        return user;
    }

}
