package ru.tsk.vkorenygin.tm.util;

import ru.tsk.vkorenygin.tm.enumerated.Role;
import ru.tsk.vkorenygin.tm.enumerated.Sort;
import ru.tsk.vkorenygin.tm.enumerated.Status;

import java.util.Arrays;

public interface EnumerationUtil {

    static Status parseStatus(final String name) {
        return Arrays
                .stream(Status.values())
                .filter(status -> status.name().equals(name))
                .findFirst().orElse(null);
    }

    static Sort parseSort(final String name) {
        return Arrays
                .stream(Sort.values())
                .filter(sort -> sort.name().equals(name))
                .findFirst().orElse(null);
    }

    static Role parseRole(final String name) {
        return Arrays
                .stream(Role.values())
                .filter(role -> role.name().equals(name))
                .findFirst().orElse(null);
    }

}
