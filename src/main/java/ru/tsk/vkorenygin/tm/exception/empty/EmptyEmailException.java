package ru.tsk.vkorenygin.tm.exception.empty;

import ru.tsk.vkorenygin.tm.exception.AbstractException;

public class EmptyEmailException extends AbstractException {

    public EmptyEmailException() {
        super("Error! E-mail is empty.");
    }

}
