package ru.tsk.vkorenygin.tm.service;

import ru.tsk.vkorenygin.tm.api.repository.IOwnerRepository;
import ru.tsk.vkorenygin.tm.api.service.IOwnerService;
import ru.tsk.vkorenygin.tm.entity.AbstractOwnerEntity;
import ru.tsk.vkorenygin.tm.exception.AbstractException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyIdException;
import ru.tsk.vkorenygin.tm.exception.entity.ProjectNotFoundException;
import ru.tsk.vkorenygin.tm.exception.system.IncorrectIndexException;
import ru.tsk.vkorenygin.tm.util.DataUtil;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public abstract class AbstractOwnerService<E extends AbstractOwnerEntity> implements IOwnerService<E> {

    protected IOwnerRepository<E> ownerRepository;

    public AbstractOwnerService(IOwnerRepository<E> repository) {
        this.ownerRepository = repository;
    }

    @Override
    public E add(final E entity) throws AbstractException {
        if (entity == null) throw new ProjectNotFoundException();
        ownerRepository.add(entity);
        return entity;
    }

    @Override
    public boolean existsById(final String id, final String userId) {
        if (DataUtil.isEmpty(id)) return false;
        return ownerRepository.existsById(id, userId);
    }

    @Override
    public boolean existsByIndex(final Integer index, final String userId) {
        if (index < 0) return false;
        return ownerRepository.existsByIndex(index, userId);
    }

    @Override
    public List<E> findAll(final String userId) {
        return ownerRepository.findAll(userId);
    }

    @Override
    public List<E> findAll(final Comparator<E> comparator, final String userId) {
        if (comparator == null) return Collections.emptyList();
        return ownerRepository.findAll(comparator, userId);
    }

    @Override
    public Optional<E> findById(final String id, final String userId) throws AbstractException {
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        return ownerRepository.findById(id, userId);
    }

    @Override
    public Optional<E> findByIndex(final int index, final String userId) throws AbstractException {
        if (index < 0) throw new IncorrectIndexException();
        return ownerRepository.findByIndex(index, userId);
    }

    @Override
    public Optional<E> removeById(final String id, final String userId) throws AbstractException {
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        return ownerRepository.removeById(id, userId);
    }

    @Override
    public Optional<E> removeByIndex(final int index, final String userId) throws AbstractException {
        if (index < 0) throw new IncorrectIndexException();
        return ownerRepository.removeByIndex(index, userId);
    }

    @Override
    public Optional<E> remove(final E entity, final String userId) throws AbstractException {
        if (entity == null) throw new ProjectNotFoundException();
        return ownerRepository.remove(entity, userId);
    }

    @Override
    public void clear(String userId) {
        ownerRepository.clear(userId);
    }

}
