package ru.tsk.vkorenygin.tm.service;

import ru.tsk.vkorenygin.tm.api.repository.IRepository;
import ru.tsk.vkorenygin.tm.api.service.IService;
import ru.tsk.vkorenygin.tm.entity.AbstractEntity;
import ru.tsk.vkorenygin.tm.exception.entity.EntityNotFoundException;
import ru.tsk.vkorenygin.tm.exception.system.IncorrectIndexException;
import ru.tsk.vkorenygin.tm.util.DataUtil;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    private final IRepository<E> repository;

    public AbstractService(IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public boolean existsById(final String id) {
        if (DataUtil.isEmpty(id))
            return false;
        return repository.existsById(id);
    }

    @Override
    public boolean existsByIndex(final Integer index) {
        if (DataUtil.isEmpty(index))
            return false;
        return repository.existsByIndex(index);
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }

    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public List<E> findAll(final Comparator<E> comparator) {
        if (comparator == null)
            return null;
        return repository.findAll(comparator);
    }

    @Override
    public Optional<E> findByIndex(final Integer index) {
        if (DataUtil.isEmpty(index))
            throw new IncorrectIndexException();
        if (index - 1 > repository.getSize())
            throw new EntityNotFoundException();
        return repository.findByIndex(index);
    }

    @Override
    public E add(final E entity) {
        if (entity == null) return null;
        return repository.add(entity);
    }

    @Override
    public void addAll(final Collection<E> collection) {
        repository.addAll(collection);
    }

    @Override
    public Optional<E> findById(final String id) {
        return repository.findById(id);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public Optional<E> removeById(final String id) {
        return repository.removeById(id);
    }

    @Override
    public Optional<E> removeByIndex(final Integer index) {
        if (DataUtil.isEmpty(index))
            throw new IncorrectIndexException();
        final Optional<E> entity = repository.findByIndex(index);
        if (!entity.isPresent())
            throw new EntityNotFoundException();
        return repository.removeByIndex(index);
    }

    @Override
    public Optional<E> remove(final E entity) {
        return repository.remove(entity);
    }

}
