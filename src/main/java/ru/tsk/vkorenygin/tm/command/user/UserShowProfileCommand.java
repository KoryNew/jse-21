package ru.tsk.vkorenygin.tm.command.user;

import ru.tsk.vkorenygin.tm.command.AbstractUserCommand;
import ru.tsk.vkorenygin.tm.entity.User;
import ru.tsk.vkorenygin.tm.exception.AbstractException;

public class UserShowProfileCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "user-show";
    }

    @Override
    public String description() {
        return "show user profile";
    }

    @Override
    public void execute() throws AbstractException {
        final User user = serviceLocator.getAuthService().getUser();
        System.out.println("[USER PROFILE]");
        show(user);
    }

}
