package ru.tsk.vkorenygin.tm.command.task;

import ru.tsk.vkorenygin.tm.command.AbstractTaskCommand;
import ru.tsk.vkorenygin.tm.entity.Task;
import ru.tsk.vkorenygin.tm.enumerated.Sort;
import ru.tsk.vkorenygin.tm.util.EnumerationUtil;
import ru.tsk.vkorenygin.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class TaskListCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-list";
    }

    @Override
    public String description() {
        return "show task list";
    }

    @Override
    public void execute() {
        final String currentUserId = serviceLocator.getAuthService().getUserId();
        System.out.println("[LIST TASKS]");
        System.out.println("ENTER SORT: ");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();
        Sort sortType = EnumerationUtil.parseSort(sort);

        List<Task> tasks;
        tasks = sortType == null ?
                serviceLocator.getTaskService().findAll(currentUserId) :
                serviceLocator.getTaskService().findAll(sortType.getComparator(), currentUserId);

        int index = 1;
        for (final Task task : tasks) {
            final String projectStatus = task.getStatus().getDisplayName();
            System.out.println(index + ". " + task + " (" + projectStatus + ")");
            index++;
        }
    }

}
