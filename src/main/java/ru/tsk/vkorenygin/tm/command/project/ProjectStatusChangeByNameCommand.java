package ru.tsk.vkorenygin.tm.command.project;

import ru.tsk.vkorenygin.tm.command.AbstractProjectCommand;
import ru.tsk.vkorenygin.tm.entity.Project;
import ru.tsk.vkorenygin.tm.enumerated.Status;
import ru.tsk.vkorenygin.tm.exception.AbstractException;
import ru.tsk.vkorenygin.tm.exception.entity.ProjectNotFoundException;
import ru.tsk.vkorenygin.tm.util.TerminalUtil;

import java.util.Arrays;

public class ProjectStatusChangeByNameCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-status-change-by-name";
    }

    @Override
    public String description() {
        return "change project status by name";
    }

    @Override
    public void execute() throws AbstractException {
        final String currentUserId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CHANGE PROJECT STATUS]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Project project = serviceLocator.getProjectService().changeStatusByName(name, status, currentUserId);
        if (project == null) throw new ProjectNotFoundException();
    }

}
