package ru.tsk.vkorenygin.tm.command.project;

import ru.tsk.vkorenygin.tm.command.AbstractProjectCommand;
import ru.tsk.vkorenygin.tm.entity.Project;
import ru.tsk.vkorenygin.tm.exception.AbstractException;
import ru.tsk.vkorenygin.tm.exception.entity.ProjectNotFoundException;
import ru.tsk.vkorenygin.tm.util.TerminalUtil;

public class ProjectStartByIdCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-start-by-id";
    }

    @Override
    public String description() {
        return "start project by id";
    }

    @Override
    public void execute() throws AbstractException {
        final String currentUserId = serviceLocator.getAuthService().getUserId();
        System.out.println("[START PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().startById(id, currentUserId);
        if (project == null) throw new ProjectNotFoundException();
    }

}
