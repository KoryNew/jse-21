package ru.tsk.vkorenygin.tm.command.task;

import ru.tsk.vkorenygin.tm.command.AbstractTaskCommand;
import ru.tsk.vkorenygin.tm.exception.AbstractException;
import ru.tsk.vkorenygin.tm.util.TerminalUtil;

public class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-update-by-index";
    }

    @Override
    public String description() {
        return "update task by index";
    }

    @Override
    public void execute() throws AbstractException {
        final String currentUserId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        serviceLocator.getTaskService().updateByIndex(index, name, description, currentUserId);
    }

}
